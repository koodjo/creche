from django.urls import reverse

from rest_framework import status


class CreateStatusRESTAPITestCaseMixin(object):
    create_status = status.HTTP_201_CREATED

    def test_create(self, data=None, **kwargs):
        response = self.get_create_response(data, **kwargs)
        self.assertEqual(response.status_code, self.create_status, getattr(response, 'data', response))
        return response


class DestroyStatusRESTAPITestCaseMixin(object):
    destroy_status = status.HTTP_204_NO_CONTENT

    def test_destroy(self, **kwargs):
        if not self.object:
            self.assertFalse(True, 'Unknown object')
        response = self.get_destroy_response(**kwargs)
        self.assertEqual(response.status_code, self.destroy_status, response.data)
        return response


class UpdateStatusRESTAPITestCaseMixin(object):
    update_status = status.HTTP_200_OK

    def test_update(self, data=None, results=None, use_patch=None, **kwargs):
        if not self.object:
            self.assertFalse(True, 'Unknown object')
        response = self.get_update_response(data, results, use_patch, **kwargs)
        self.assertEqual(response.status_code, self.update_status, response.data)
        return response


class ListStatusRESTAPITestCaseMixin(object):
    list_status = status.HTTP_200_OK

    def test_list(self, **kwargs):
        response = self.get_list_response(**kwargs)
        self.assertEqual(response.status_code, self.list_status, response.data)
        results = response.data
        if self.list_status == status.HTTP_200_OK:
            if self.pagination_results_field:
                self.assertIn(self.pagination_results_field, response.data)
                results = results[self.pagination_results_field]
            self.assertTrue(len(results) >= 1)
        return response


class DetailStatusRESTAPITestCaseMixin(object):
    detail_status = status.HTTP_200_OK

    def test_detail(self, **kwargs):
        if not self.object:
            self.assertFalse(True, 'Unknown object')
        response = self.get_detail_response(**kwargs)
        self.assertEqual(response.status_code, self.detail_status, response.data)
        self._check_attributes(response.data)
        return response


class MethodRESTAPITestCaseMixin(object):

    method_name = None

    def get_method_name(self):
        if not self.method_name:
            raise NotImplementedError('Property <method_name> should be defined in the TestCase')
        if self.base_name:
            return '-' + self.method_name if self.method_name else ''
        else:
            return self.method_name or ''

    def get_url_args(self):
        return []

    def get_detail_url(self):
        return reverse(self.base_name + (self.get_method_name() or self.LIST_SUFFIX), args=self.get_url_args())

    def get_list_url(self):
        return reverse(self.base_name + (self.get_method_name() or self.DETAIL_SUFFIX), args=self.get_url_args())

    def get_create_url(self):
        return reverse(self.base_name + (self.get_method_name() or self.LIST_SUFFIX), args=self.get_url_args())

    def get_update_url(self):
        return reverse(self.base_name + (self.get_method_name() or self.DETAIL_SUFFIX), args=self.get_url_args())

    def get_destroy_url(self):
        return reverse(self.base_name + (self.get_method_name() or self.DETAIL_SUFFIX), args=self.get_url_args())


class ReadStatusRESTAPITestCaseMixin(DetailStatusRESTAPITestCaseMixin, ListStatusRESTAPITestCaseMixin):
    pass


class WriteStatusRESTAPITestCaseMixin(CreateStatusRESTAPITestCaseMixin, UpdateStatusRESTAPITestCaseMixin, DestroyStatusRESTAPITestCaseMixin):
    pass


class ReadWriteStatusRESTAPITestCaseMixin(ReadStatusRESTAPITestCaseMixin, WriteStatusRESTAPITestCaseMixin):
    pass


class FilterRESTAPITestCaseMixin(object):
    batch_size = 10

    def setUp(self):
        super(FilterRESTAPITestCaseMixin, self).setUp()
        self.objects = self.get_objects(self.get_factory_class())

    def get_objects(self, factory):
        return factory.create_batch(self.batch_size)

    def test_filter(self, **kwargs):
        response = self.get_list_response()

        results = response.data

        if self.pagination_results_field:
            self.assertIn(self.pagination_results_field, response.data)
            results = results[self.pagination_results_field]

        self.assertTrue(len(results) >= self.batch_size)
        return response
