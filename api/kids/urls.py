from django.urls import path, include

from rest_framework_nested import routers

from api.kids import viewsets

router = routers.DefaultRouter()

router.register(r'klasses', viewsets.KlassViewSet)
router.register(r'kids', viewsets.KidViewSet)
router.register(r'logs', viewsets.LogViewSet)

urlpatterns = [
    path(r'', include(router.urls)),
]
