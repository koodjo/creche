import factory

from django.core import management

from rest_framework import status
from rest_framework.test import APITestCase, APIRequestFactory
from rest_framework.test import force_authenticate
from rest_framework.authtoken.models import Token
from rest_assured.testcases import ReadWriteRESTAPITestCaseMixin, ReadRESTAPITestCaseMixin, BaseRESTAPITestCase

from api.utils import test_mixins

from . import factories


def setUpModule():
    pass


def tearDownModule():
    management.call_command('flush', verbosity=0, interactive=False)


class KlassAPITestCaseMixin(test_mixins.ReadWriteStatusRESTAPITestCaseMixin,
                            test_mixins.FilterRESTAPITestCaseMixin,
                            ReadWriteRESTAPITestCaseMixin):
    base_name = 'klass'
    pagination_results_field = 'results'
    factory_class = factories.KlassFactory
    create_status = status.HTTP_403_FORBIDDEN
    update_status = status.HTTP_403_FORBIDDEN
    destroy_status = status.HTTP_403_FORBIDDEN
    list_status = status.HTTP_200_OK
    detail_status = status.HTTP_200_OK
    update_data = {}
    attributes_to_check = []

    def get_create_data(self):
        return factory.build(dict, FACTORY_CLASS=factories.KlassFactory)


class KlassAuthAPITestCaseMixin(KlassAPITestCaseMixin):
    create_status = status.HTTP_405_METHOD_NOT_ALLOWED
    update_status = status.HTTP_405_METHOD_NOT_ALLOWED
    destroy_status = status.HTTP_405_METHOD_NOT_ALLOWED
    user_factory = factories.UserFactory

    def test_auth(self, **kwargs):
        self.user = self.user_factory.create()

        self.client.force_authenticate(self.user)


class KidAPITestCaseMixin(test_mixins.ReadWriteStatusRESTAPITestCaseMixin,
                          test_mixins.FilterRESTAPITestCaseMixin,
                          ReadWriteRESTAPITestCaseMixin):
    base_name = 'kid'
    pagination_results_field = 'results'
    factory_class = factories.KidFactory
    create_status = status.HTTP_403_FORBIDDEN
    update_status = status.HTTP_403_FORBIDDEN
    destroy_status = status.HTTP_403_FORBIDDEN
    list_status = status.HTTP_200_OK
    detail_status = status.HTTP_200_OK
    update_data = {'is_learner': True}
    attributes_to_check = []

    def get_create_data(self):
        return self.factory_class.get_create_data()


class KidAuthAPITestCaseMixin(KidAPITestCaseMixin):
    create_status = status.HTTP_201_CREATED
    update_status = status.HTTP_200_OK
    destroy_status = status.HTTP_405_METHOD_NOT_ALLOWED
    user_factory = factories.UserFactory


class LogAPITestCaseMixin(test_mixins.ReadWriteStatusRESTAPITestCaseMixin,
                          test_mixins.FilterRESTAPITestCaseMixin,
                          ReadWriteRESTAPITestCaseMixin):
    base_name = 'kid'
    pagination_results_field = 'results'
    factory_class = factories.LogFactory
    create_status = status.HTTP_403_FORBIDDEN
    update_status = status.HTTP_403_FORBIDDEN
    destroy_status = status.HTTP_403_FORBIDDEN
    list_status = status.HTTP_200_OK
    detail_status = status.HTTP_200_OK
    update_data = {}
    attributes_to_check = []

    def get_create_data(self):
        return self.factory_class.get_create_data()


class LogAuthAPITestCaseMixin(LogAPITestCaseMixin):
    create_status = status.HTTP_201_CREATED
    update_status = status.HTTP_200_OK
    destroy_status = status.HTTP_405_METHOD_NOT_ALLOWED
    user_factory = factories.UserFactory


class Test_1_Klass(KlassAPITestCaseMixin, BaseRESTAPITestCase):
    pass


class Test_2_Klass(KlassAuthAPITestCaseMixin, BaseRESTAPITestCase):
    pass


class Test_3_Kid(KidAPITestCaseMixin, BaseRESTAPITestCase):
    pass


class Test_4_Kid(KidAuthAPITestCaseMixin, BaseRESTAPITestCase):
    pass


class Test_5_Log(LogAPITestCaseMixin, BaseRESTAPITestCase):
    pass


class Test_6_Log(LogAuthAPITestCaseMixin, BaseRESTAPITestCase):
    pass
