import django_filters.rest_framework
from rest_framework import mixins, viewsets, filters, permissions

from kids.models import Klass, Kid, Log

from api.kids import serializers as kids_serializers
from api.kids import filters as kids_filters


class KlassViewSet(mixins.ListModelMixin,
                   mixins.RetrieveModelMixin,
                   viewsets.GenericViewSet):

    serializer_class = kids_serializers.KlassSerializer
    queryset = Klass.objects.all()
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
    )
    filter_backends = (
        django_filters.rest_framework.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter
    )
    filter_class = kids_filters.KlassFilter
    search_fields = ('name', )


class KidViewSet(mixins.ListModelMixin,
                 mixins.RetrieveModelMixin,
                 mixins.UpdateModelMixin,
                 mixins.CreateModelMixin,
                 viewsets.GenericViewSet):

    serializer_class = kids_serializers.KidSerializer
    queryset = Kid.objects.all()
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
    )
    filter_backends = (
        django_filters.rest_framework.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter
    )
    filter_class = kids_filters.KidFilter
    search_fields = ('first_name', 'last_name', 'middle_name')


class LogViewSet(mixins.ListModelMixin,
                 mixins.RetrieveModelMixin,
                 mixins.UpdateModelMixin,
                 mixins.CreateModelMixin,
                 mixins.DestroyModelMixin,
                 viewsets.GenericViewSet):

    serializer_class = kids_serializers.LogSerializer
    queryset = Log.objects.all()
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
    )
    filter_backends = (
        django_filters.rest_framework.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter
    )
    filter_class = kids_filters.LogFilter
    search_fields = ('kid__first_name', 'kid__last_name', 'kid__middle_name')
