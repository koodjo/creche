# -*- coding: utf-8 -*-
import django_filters

from kids.models import Klass, Kid, Log


class KlassFilter(django_filters.FilterSet):

    class Meta:
        model = Klass
        fields = ('name', )


class KidFilter(django_filters.FilterSet):

    class Meta:
        model = Kid
        fields = ('is_learner', 'klass', 'birthday')


class LogFilter(django_filters.FilterSet):
    is_learner = django_filters.BooleanFilter(name='kid__is_learner')

    class Meta:
        model = Log
        fields = ('kid', 'who_lead_away', 'who_take_away', 'date_lead', 'date_away', 'creation_date', )
