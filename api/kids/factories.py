# -*- coding: utf-8 -*-
import datetime
import factory
from factory import fuzzy

from django.contrib.auth.models import User

from kids.models import Klass, Kid, Log


class UserFactory(factory.DjangoModelFactory):

    first_name = fuzzy.FuzzyText(length=255)
    last_name = fuzzy.FuzzyText(length=255)
    username = factory.sequence(lambda n: 'account{0}@example.com'.format(n))
    email = factory.sequence(lambda n: 'account{0}@example.com'.format(n))
    is_active = True

    class Meta:
        model = User


class KlassFactory(factory.DjangoModelFactory):
    name = fuzzy.FuzzyText(length=255)

    class Meta:
        model = Klass


class KidFactory(factory.DjangoModelFactory):

    first_name = fuzzy.FuzzyText(length=255)
    last_name = fuzzy.FuzzyText(length=255)
    middle_name = fuzzy.FuzzyText(length=255)
    birthday = fuzzy.FuzzyDate(datetime.date(2015, 1, 1))
    klass = factory.SubFactory(KlassFactory)
    is_learner = fuzzy.FuzzyInteger(0, 1)

    class Meta:
        model = Kid

    @classmethod
    def get_create_data(cls):
        data = factory.build(dict, FACTORY_CLASS=KidFactory)
        data['klass'] = KlassFactory.create().pk
        return data


class LogFactory(factory.DjangoModelFactory):
    kid = factory.SubFactory(KidFactory)
    who_lead_away = fuzzy.FuzzyChoice(Log.WHO_TYPES._db_values)
    who_take_away = fuzzy.FuzzyChoice(Log.WHO_TYPES._db_values)
    date_lead = fuzzy.FuzzyDate(datetime.date(2015, 1, 1))
    date_away = fuzzy.FuzzyDate(datetime.date(2015, 1, 1))
    creation_date = fuzzy.FuzzyDate(datetime.date(2015, 1, 1))

    class Meta:
        model = Log

    @classmethod
    def get_create_data(cls):
        data = KidFactory.get_create_data()
        data['kid'] = KidFactory.create().pk
        return data
