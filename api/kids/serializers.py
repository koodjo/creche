from rest_framework import serializers

from kids.models import Klass, Kid, Log


class KlassSerializer(serializers.ModelSerializer):

    class Meta:
        model = Klass
        fields = '__all__'


class KidSerializer(serializers.ModelSerializer):
    photo = serializers.ImageField(use_url=False, required=False)

    class Meta:
        model = Kid
        fields = '__all__'


class LogSerializer(serializers.ModelSerializer):

    class Meta:
        model = Log
        fields = '__all__'
