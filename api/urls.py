from django.urls import path, include

urlpatterns = [
    path('creche/', include('api.kids.urls')),
]
