from creche.settings import *

DEBUG = False

# make tests faster
SOUTH_TESTS_MIGRATE = False
DATABASES = {
    'default': {
        'ENGINE': "django.db.backends.sqlite3",
        'NAME': "test",
        'HOST': "localhost",
        'PORT': "",
    }
}

INSTALLED_APPS += ('django_jenkins',)

JENKINS_TASKS = ('django_jenkins.tasks.run_pylint',
                 'django_jenkins.tasks.run_pep8',
                 'django_jenkins.tasks.run_pyflakes',
                 'django_jenkins.tasks.with_coverage')
