from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as l_


class KidsConfig(AppConfig):
    name = 'kids'
    verbose_name = l_('Kids')
