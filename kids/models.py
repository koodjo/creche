import uuid

from django.db import models
from django.utils.translation import ugettext_lazy as l_

from model_utils import Choices


def upload_to_photo(instance, filename):
    ext = filename.split('.')[-1]
    filename = "{}.{}".format(uuid.uuid4(), ext)
    return u'{}/{}'.format(instance.BASE_PHOTO_UPLOAD_DIR, filename)


class Klass(models.Model):

    name = models.CharField(max_length=255, verbose_name=l_('Название'))

    class Meta:
        verbose_name = l_(u'Класс')
        verbose_name_plural = l_(u'Классы')

    def __str__(self):
        return self.name


class Kid(models.Model):
    BASE_PHOTO_UPLOAD_DIR = 'photo'

    TYPES = Choices(
        (0, 'mother', l_(u'Мать')),
        (1, 'father', l_(u'Отец')),
        (100, 'other', l_(u'Другой')),
    )

    photo = models.ImageField(upload_to=upload_to_photo, blank=True, null=True,
                              verbose_name=l_('Фото'))

    first_name = models.CharField(max_length=255, verbose_name=l_('Имя'))

    last_name = models.CharField(max_length=255, verbose_name=l_('Фамилия'))

    middle_name = models.CharField(max_length=255, null=True, blank=True,
                                   verbose_name=l_('Отчество'))

    birthday = models.DateField(db_index=True, verbose_name=l_('День рождения'))

    klass = models.ForeignKey('Klass', blank=True, null=True, related_name='kids',
                              on_delete=models.DO_NOTHING, verbose_name=l_(u'Класс'))

    is_learner = models.BooleanField(db_index=True, default=True, verbose_name=l_(u'Учится'))

    class Meta:
        verbose_name = l_(u'Ребенок')
        verbose_name_plural = l_(u'Дети')

    def __str__(self):
        return ' '.join(filter(lambda x: x, [self.last_name, self.first_name, self.middle_name]))


class Log(models.Model):

    WHO_TYPES = Choices(
        (0, 'mother', l_(u'Мать')),
        (1, 'father', l_(u'Отец')),
        (100, 'other', l_(u'Другой')),
    )

    kid = models.ForeignKey('Kid', on_delete=models.DO_NOTHING, related_name='logs',
                            verbose_name=l_(u'Ребенок'))

    who_lead_away = models.PositiveSmallIntegerField(choices=WHO_TYPES, db_index=True,
                                                     verbose_name=l_('Когда привел'))

    who_take_away = models.PositiveSmallIntegerField(choices=WHO_TYPES, db_index=True,
                                                     verbose_name=l_('Когда забрал'))

    date_lead = models.DateTimeField(db_index=True, verbose_name=l_('Когда привел'))

    date_away = models.DateTimeField(db_index=True, verbose_name=l_('Когда забрал'))

    creation_date = models.DateTimeField(db_index=True, auto_now_add=True, verbose_name=l_('Когда забрал'))

    class Meta:
        verbose_name = l_(u'Журнал')
        verbose_name_plural = l_(u'Журнал')

    def __str__(self):
        return '{} ({} - {})'.format(self.kid, self.date_lead, self.date_away)
