from django.contrib import admin

from kids.models import Klass, Kid, Log

admin.site.register(Klass)
admin.site.register(Kid)
admin.site.register(Log)
